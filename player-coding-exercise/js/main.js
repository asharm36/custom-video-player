function togglevid()
{
	var video = document.getElementById("vid");
	var play_status = document.getElementById("play");

	if(video.paused)
	{
		video.play();
		play.className = 'fa fa-pause';
	}

	else
	{
		video.pause();
		play.className = 'fa fa-play';
	}
}

function progChange()
{
	var prog = document.getElementById("progress");
	var video = document.getElementById("vid");

	var progress_at = video.duration * (prog.value/100);
	video.currentTime = progress_at;

	//buffer functionality
	var curr_buf = video.buffered.end(0);
	var buf_percent = 100*curr_buf/video.duration;
	prog.style.background = 'linear-gradient(to right,  #cccccc 0%,#cccccc '+buf_percent+'%,#666 '+buf_percent+'%,#666 100%)';	
}

function updateProgress()
{
	var prog = document.getElementById("progress");
	var video = document.getElementById("vid");
	var curr_t = document.getElementById("currTime");
	var end_t = document.getElementById("TotalTime");
	
	var newTime = video.currentTime * (100/video.duration);
	prog.value = newTime;

	var curr_min = Math.floor(video.currentTime/60);
	var curr_s = Math.floor(video.currentTime - curr_min * 60);
	var total_min = Math.floor(video.duration/60);
	var total_s = Math.floor(video.duration - total_min * 60);

	if(total_s < 10) total_s = "0" + total_s;
	if(total_min < 10) total_min = "0" + total_min;
	if(curr_s < 10) curr_s = "0" + curr_s;
	if(curr_min < 10) curr_min = "0" + curr_min;

	currTime.innerHTML = curr_min + ":" + curr_s;
	TotalTime.innerHTML = total_min + ":" + total_s;

	//buffer functionality
	var curr_buf = video.buffered.end(0);
	var buf_percent = 100*curr_buf/video.duration;
	prog.style.background = 'linear-gradient(to right,  #4ebbff 0%,#4ebbff '+buf_percent+'%,#666 '+buf_percent+'%,#666 100%)';
}

function togglesound()
{
	var video = document.getElementById("vid");
	var mute = document.getElementById("mute");
	var soundLv = document.getElementById("soundLv");

	if(video.muted)
	{
		video.muted = false;
		mute.className = 'fa fa-volume-up';
		soundLv.value = 100;
	}

	else
	{
		video.muted = true;
		mute.className = 'fa fa-volume-off';
		soundLv.value = 0;
	}
}

function soundChange()
{
	var soundLv = document.getElementById("soundLv");
	var video = document.getElementById("vid");

	video.volume = soundLv.value/100;
}

function togglescreen()
{
	var video = document.getElementById("vid");
	var fs = document.getElementById("fs");

	if(video.webkitRequestFullScreen())
		video.webkitRequestFullScreen();
	else if(video.requestFullScreen())
		video.requestFullScreen();
	else
		video.mozRequestFullScreen();
}
